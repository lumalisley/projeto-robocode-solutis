<p aling= "Left">
<img src="https://cinepop.com.br/wp-content/uploads/2020/04/jaws-cinepop1.jpg"
height="350" width="1000">
</p>





# __**SharkGirl**__


## README

☕︎ Java 

### **Explicação do código:**

SharkGirl é um robô iniciante, mas com os instintos de tubarão vai atrás dos seus oponentes ficando cara a cara e atirando com força total.


### **Pontos Fortes:**


✔ O scanner permanecer na direção do inimigo com posição perpendicular fazendo com que a arma esteja sempre apontada ao oponente, calculando a energia inicial menos a atual e atirando sem parar.

✔ Quando a distância do inimigo aumenta, a força do tiro diminui para gastar menos energia. 


<p align="center">
    <img src="https://www.imagensanimadas.com/data/media/516/tubarao-imagem-animada-0047.gif"
height="150" width="200">
</p>


### **Pontos Fracos:**

✔ Por ter um scanner e arma apontada diretamente para inimigo e sempre lançando balas, quando o oponente desvia ele acaba gastando energia desnecessária.

✔ Por sempre está atrás do inimigo a chance de colidir e ser alvejado por fireMAX é maior.

<p aling="Right">
    <img src="https://media.tenor.com/images/99d338b10da35c734efdee038254e4b2/tenor.gif"
height="150" width="180">
</p>

### **Considerações Pessoais:**

Por ser formada em engenharia, nunca tive contato com programação em Java, apenas em C++, foi uma experiência surpreendente e amor à primeira vista quando comecei a aprender. 
Com o processo seletivo aprendi a usar o GitHub, GitLab, GitBash participar de foruns de dúvidas e ver resultado com o básico que consegui aprender com o robocode, ou seja, aprendi variás coisas com o programa Talent Sprint Solutis em um curto espaço de tempo.


_E para finalizar, o robô SharkGirl ainda comemora com uma dancinha suas vitórias._ 



<p aling="center">
    <img src="http://gallery.tinyletterapp.com/a269bccb21989dee10f714c448455678572daa1f/images/9eb26d0f-e4ca-4788-8976-7d73331bc653.gif"
height="200" width="200">
    </p>