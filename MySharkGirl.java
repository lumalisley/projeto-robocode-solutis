package sharkGirll;
import robocode.*;
import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;

//SharkGirl - robô criado por (Luma Lisley)
//o_o



public class MySharkGirl extends AdvancedRobot {

	double enemyPreviousEnergy = 120;
	int direction = 1;
	int gun = 3;
	int wallHitBox = 60;

	public void run() {
			setColors(Color.pink,Color.pink,Color.black);
			setBodyColor(Color.pink);
			setGunColor(Color.pink);
			setRadarColor(Color.black);
			
		setTurnGunRight(99999);

	}

	public void onScannedRobot(ScannedRobotEvent e) {
			// Seta posição do robo perpendicular a do inimigo 
		setTurnRight(e.getBearing() + 90 - 30 * direction);

			// Se o inimigo atirar, ele vai gastar energia
			// Calcula a energia inicial menos a energia atual
			// Se for maior que zero e menor ou igual a 3, é porque ele atirou
		double energy = enemyPreviousEnergy - e.getEnergy();
		if (energy > 0 && energy <= 3) {
			// Muda de direção
			direction = -direction;
			setAhead((e.getDistance()) * direction);
		}
		
		
		gun = -gun;
		setTurnRadarRight(360 * gun);
			// Encontra a diferença do heading do robo e do heading do canhao e soma com o bearing do inimigo
			// Isso faz com que a arma esteja sempre apontada pro inimigo
		setTurnGunRight(getHeading() - getGunHeading() + e.getBearing());

			// Quando a distância inimiga aumenta, a força do tiro diminui, para gastar menos energia
		setFire(400 / e.getDistance());
		
			// Grava a energia para o próximo scan
		enemyPreviousEnergy = e.getEnergy();

	}

	

	public void onWin(WinEvent event) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
			turnRight(36000);
		}
	}

}